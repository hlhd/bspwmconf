
# Hilal Hadyanto's Simple X hotkey daemon config file
# 
# useful link
# https://github.com/baskerville/sxhkd/blob/master/doc/sxhkd.1.asciidoc
# =============================================================================#

# System utilities bindings
# ============================================================================= 
# Screen brightness control
# -----------------------------------------------------------------------------
XF86MonBrightness{Up,Down}
    /home/hianto/bin/monbr_{up,down}.sh

# -----------------------------------------------------------------------------
super + XF86MonBrightness{Up,Down}
    light -s "sysfs/backlight/intel_backlight" {-S 100,-S 3}

# -----------------------------------------------------------------------------
super + s: {bracketleft,bracketright,ctrl + bracketleft,ctrl + bracketright}
    { \
    /home/hianto/bin/monbr_down.sh, \
    /home/hianto/bin/monbr_up.sh, \
    light -s "sysfs/backlight/intel_backlight" -S 3, \
    light -s "sysfs/backlight/intel_backlight" -S 100, \
    }

# Audio control
# -----------------------------------------------------------------------------
XF86Audio{Raise,Lower}Volume
    amixer -qc 0 -- sset Master {2%+,2%-}

# -----------------------------------------------------------------------------
XF86AudioMute
    amixer -qD pulse -- sset Master toggle

# Screen capture
# -----------------------------------------------------------------------------
Print
    flameshot full -p ~/Screenshots/ -d 1000

# -----------------------------------------------------------------------------
super + Print
    flameshot gui -p ~/Screenshots/ -d 1000

# -----------------------------------------------------------------------------
super + shift + Print
    flameshot screen -p ~/Screenshots/ -d 1000

# -----------------------------------------------------------------------------
super + x; {b,a,x,shift + b,shift + a}
    { \
    /home/hianto/bin/makeup_ss_bg b, \
    /home/hianto/bin/makeup_ss_auth b, \
    /home/hianto/bin/makeup_ss_alpha, \
    /home/hianto/bin/makeup_ss_bg g, \
    /home/hianto/bin/makeup_ss_auth g, \
    }


# Control the power
# -----------------------------------------------------------------------------
super + control + p; {o,r,s,h}
    systemctl {poweroff,reboot,suspend,hibernate}

# Screen locking
# -----------------------------------------------------------------------------
super + apostrophe
    xautolock -locknow

# App and launcher bindings
# =============================================================================
# Flatpak app launch bindings
super + o; {l,g,i,k}
    flatpak run { \
    org.libreoffice.LibreOffice, \
    org.gimp.GIMP, \
    org.inkscape.Inkscape, \
    org.kde.krita, \
    }

# App launch bindings
# -----------------------------------------------------------------------------
super + Return
    tabbed -c x-terminal-emulator -embed

# -----------------------------------------------------------------------------
super + a; {f,e,s,g}
    { \
    firefox, \
    thunar, \
    scrcpy -S -b 2M -m 720, \
    gpick, \
    }

# rofi / dmenu
# =============================================================================
super + r; {d,r,w,p,e}
    { \
    rofi -show drun, \
    rofi -show run /home/hianto/bin/, \
    rofi -show window, \
    /home/hianto/bin/rofi-powermenu.sh, \
    rofimoji, \
    }

# control the environment
# =============================================================================
super + e; {l,ctrl + l,alt + l,c,ctrl + b,s,ctrl + s,x,d}
    { \
    xautolock -locknow, \
    xautolock -disable && notify-send "Xautolock Disabled", \
    xautolock -enable && notify-send "Xautolock Enabled", \
    /home/hianto/bin/own_script_toggle_compton, \
    /home/hianto/bin/launch_polybar, \
    /home/hianto/bin/own_script_toggle_stalonetray, \
    /home/hianto/bin/own_script_restart_sxhkd bspwm, \
    /home/hianto/bin/own_script_restart_xautolock, \
    /home/hianto/bin/own_script_restart_dunst && notify-send "DUNST restarted", \
    }

# -----------------------------------------------------------------------------
super + b
    /home/hianto/bin/bar_off

# -----------------------------------------------------------------------------
@super + b
    /home/hianto/bin/bar_on

# utilities
# =============================================================================
super + u; {w,t,m}
    { \
    /home/hianto/bin/wallpaper_mager, \
    /home/hianto/bin/dmenu_translate, \
    /home/hianto/bin/monitor_setup, \
    }

super + p
    passmenu

# clipboard manager - copyq
# =============================================================================
super + semicolon; {s,alt + s,d,e}
    { \
    copyq show, \
    copyq hide, \
    copyq disable && notify-send "CopyQ - Clipboard storing disabled", \
    copyq enable && notify-send "CopyQ - Clipboard storing enabled", \
    }

# =============================================================================
# EOF
