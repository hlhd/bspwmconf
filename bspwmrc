#!/usr/bin/env bash
#
# Hilal Hadyanto's BSPWM configuration file
# ============================================================================= 
# Truncate common command that are used herein.
# -----------------------------------------------------------------------------
_bc() {
    bspc config "$@"
}

_check() {
	command -v "$1" > /dev/null
}

# Variables
# -----------------------------------------------------------------------------
script_folder=/home/hianto/bin
monitor_count=$(xrandr | grep \* | wc -l)

# Call a module
# -----------------------------------------------------------------------------
DIR=$(dirname "$0")

. ${DIR}/bspwm_color
if [ "$monitor_count" = '2' ]; then
    notify-send "Monitor count is $monitor_count applying multihead window rules"
    . ${DIR}/bspwm_rules_dual
else
    notify-send "Monitor count is $monitor_count applying single monitor window rules"
    . ${DIR}/bspwm_rules
fi
. ${DIR}/bspwm_autostart

# =============================================================================
#
# Things to bear in mind about certain configurations:
#
# 1. single_monocle true = Does not recognise receptacles, so if you have a
#    single window and insert a receptacle, the window will remain in monocle view.
#    Desired behaviour is to tile accordingly.
#    -----
#    from man bspc:
#    set the dekstop layout to monocle if there's only one tiled window in the tree.
#
#
# 2. borderless_monocle true = smart border are bad in multi-monitor setups
#    because you cannot tell which window has focus when a monocle exist in each monitor.
#    Single monitor is okay
#    -----
#    from man bspc:
#    remove borders of tiled windows for the monocle desktop layout
#
#
# 3. ignore_ewmh_focus true = This is very opinionated as it completely prevents
#    focus stealing. Adapt accordingly.
#    -----
#    from man bspc:
#    ignore EWMH focus requests coming form applications
#
#
# 4. pointer_modifier mod1 = enables mouse actions while holding down the Alt key
#    -----
#    from man bspc:
#    the behaviour of pointer_modifier + button<n> can be modified through
#    the pointer_action<n> setting.
#
#
# 5. pointer_action1 move = Alt + Right Click allows you to drag a floating window
#    Also work for switching position for tiled windows, if that is your thing.
#
#
# 6. pointer_action2 resize_side = Alt + Left Click allows you to resize the nearest
#    side of the focused window. Work for both tiled and floating.
#
# =============================================================================

# Java app fix
# =============================================================================
wmname LG3D

# Behaviour Configurations
# -----------------------------------------------------------------------------
_bc border_width 2
_bc window_gap 0
_bc split_ratio 0.50
_bc single_monocle false
_bc borderless_monocle false
_bc gapless_monocle false
_bc paddingless_monocle false
_bc top_padding 25

# set cursor to pointer left
xsetroot -cursor_name left_ptr

# Window Management Configurations
# -----------------------------------------------------------------------------
_bc focus_follows_pointer false
_bc pointer_modifier mod4
_bc pointer_action1 move
_bc pointer_action2 resize_side
_bc click_to_focus any
_bc swallow_first_click false
_bc initial_polarity first_child
_bc ignore_ewmh_focus true

# Per host configurations
# =============================================================================
# I deploy my dotfiles on two separate machines.  On one I often run a
# dual monitor setup.  As such, there are some bespoke configs for each
# host.  Settings such as workspaces [per monitor] and program-specific
# window rules are included.
_check $script_folder/own_script_bspwm_per_host_configs && $script_folder/own_script_bspwm_per_host_configs &

# BSPWM state
# =============================================================================
#if [ -f /home/hianto/bspwm-session.txt ]; then
#    notify-send "BSPWM session is found, loading session"
#    bspc wm --load-state /home/hianto/session.txt
#fi

