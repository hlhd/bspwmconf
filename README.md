# My BSPWM Config

This [BSPWM](https://github.com/baskerville/bspwm) config I built based on [Protesilaos Stavrou's dotfile for debian 10](https://protesilaos.com/books/2019-07-02-pdfd/).  

## Preview

 These screenshots is taken on december 2020
 
 ![Desktop](https://gitlab.com/hi.anto/bspwmconf/-/raw/main/preview/01.png)
 ![Vim with bspwm config open](https://gitlab.com/hi.anto/bspwmconf/-/raw/main/preview/02.png)
 ![Desktop with gaps](https://gitlab.com/hi.anto/bspwmconf/-/raw/main/preview/03.png)
 ![Firefox](https://gitlab.com/hi.anto/bspwmconf/-/raw/main/preview/04.png)
 ![Dmenu](https://gitlab.com/hi.anto/bspwmconf/-/raw/main/preview/05.png)
 ![scrcpy](https://gitlab.com/hi.anto/bspwmconf/-/raw/main/preview/06.png)
